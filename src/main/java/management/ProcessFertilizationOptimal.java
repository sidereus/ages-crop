
package management;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessFertilizationOptimal {
    public double nmin;
    public double BioNOpt;
    public double BioNAct;
    public double FPHUact;
    public double bion02;
    public double bion04;
    public double bion06;
    public double bion08;
    public double endbioN;

    public double nfert;
    public double restfert;

    public double fertNH4;
    public double fertNO3;
    public double fertorgNactive;
    public double fertorgNfresh;

    public void processFertilizationOptimal() {
        double run_nfert = nfert;
        double fertNH4_ = 0;
        double fertNO3_ = 0;
        double fertNactive = 0;
        double fertNfresh = 0;

        // manure (nfert = 0); 15/15/15 (nfert = 1,2); urea (nfert = 3)
        if (run_nfert == 0) {
            fertNH4_ = 0.01 * 0.99;
            fertNO3_ = 0.01 * 0.01;
            fertNactive = 0.015;
            fertNfresh = 0.015;
        } else if (run_nfert < 3) {
            fertNH4_ = 0;
            fertNO3_ = 0.15;
            fertNactive = 0;
            fertNfresh = 0;
        } else if (run_nfert == 3) {
            fertNH4_ = 0.43;
            fertNO3_ = 0;
            fertNactive = 0;
            fertNfresh = 0;
        }

        double targetN = 0;
        double fertN_total = fertNH4_ + fertNO3_ + fertNactive + fertNfresh;

        if (FPHUact > 0 && FPHUact <= 0.2) {
            targetN = (bion02 * FPHUact) / 0.2;
        } else if (FPHUact > 0.2 && FPHUact <= 0.4) {
            targetN = (((bion04 - bion02) * FPHUact) / 0.4) + bion02;
        } else if (FPHUact > 0.4 && FPHUact <= 0.6) {
            targetN = (((bion06 - bion04) * FPHUact) / 0.6) + bion04;
        } else if (FPHUact > 0.6 && FPHUact <= 0.8) {
            targetN = (((bion08 - bion06) * FPHUact) / 0.8) + bion06;
        } else if (FPHUact > 0.8 && FPHUact <= 1) {
            targetN = (((endbioN - bion08) * FPHUact) / 1) + bion08;
        }

        if (BioNOpt < targetN) {
            endbioN = endbioN - (targetN - BioNOpt);
        }

        double demand_factor = Math.min(Math.sqrt(FPHUact) + 0.1, 1);
        double future_demand = (demand_factor * endbioN) - BioNOpt;
        double actual_demand = BioNOpt - BioNAct;
        double total_demand = (future_demand + actual_demand) - nmin;

        double famount = total_demand / fertN_total;
        if (famount < 0) {
            famount = 0;
        }

        double Namount = famount * fertN_total;
        double run_restfert = restfert;

        if (run_nfert > 0) {
            if (Namount < run_restfert) {
            } else {
                Namount = run_restfert;
            }
        }

        run_restfert = run_restfert - Namount;
        famount = Namount / fertN_total;

        if (FPHUact > 0.95) {
            famount = 0;
        }

        run_restfert = Math.max(run_restfert, 0);
        run_nfert++;

        nfert = run_nfert;
        fertNH4 = famount * fertNH4_;
        fertNO3 = famount * fertNO3_;
        fertorgNfresh = famount * fertNfresh;
        fertorgNactive = famount * fertNactive;
        restfert = run_restfert;
    }
}
