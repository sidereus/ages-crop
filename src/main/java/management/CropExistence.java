
package management;

/**
 *
 * @author Nathan Lighthart
 */
public class CropExistence {
    public static boolean isPerennialCrop(int cropType) {
        switch (cropType) {
            case 3:
            case 6:
            case 7:
                return true;
            default:
                return false;
        }
    }

    public static boolean doesHarvestLeaveCrop(int harvestType) {
        switch (harvestType) {
            case 2:
                return true;
            default:
                return false;
        }
    }
}
