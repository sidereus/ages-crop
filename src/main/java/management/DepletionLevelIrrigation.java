
package management;

import java.time.MonthDay;

/**
 *
 * @author Nathan Lighthart
 */
public class DepletionLevelIrrigation extends Irrigation {
    private final MonthDay start;
    private final MonthDay end;
    private final double depletionTriggerLevel;
    private final double depletionRefillLevel;

    public DepletionLevelIrrigation(int irrigationId, double rate, double depth, MonthDay start, MonthDay end, double depletionTriggerLevel, double depletionRefillLevel) {
        super(irrigationId, rate, depth);
        this.start = start;
        this.end = end;
        this.depletionTriggerLevel = depletionTriggerLevel;
        this.depletionRefillLevel = depletionRefillLevel;
    }

    public MonthDay getStart() {
        return start;
    }

    public MonthDay getEnd() {
        return end;
    }

    public double getDepletionTriggerLevel() {
        return depletionTriggerLevel;
    }

    public double getDepletionRefillLevel() {
        return depletionRefillLevel;
    }
}
