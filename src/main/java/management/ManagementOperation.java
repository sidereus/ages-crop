
package management;

import crop.Crop;
import java.time.MonthDay;

/**
 *
 * @author Nathan Lighthart
 */
public abstract class ManagementOperation {
    private final int managementId;
    private final Crop crop;
    private final MonthDay date;

    public ManagementOperation(int managementId, Crop crop, MonthDay date) {
        this.managementId = managementId;
        this.crop = crop;
        this.date = date;
    }

    public int getManagementId() {
        return managementId;
    }

    public Crop getCrop() {
        return crop;
    }

    public MonthDay getDate() {
        return date;
    }
}
