
package management;

import crop.Crop;
import java.time.MonthDay;

/**
 *
 * @author Nathan Lighthart
 */
public class FallowOperation extends ManagementOperation {
    public FallowOperation(int managementId, Crop crop, MonthDay date) {
        super(managementId, crop, date);
    }
}
