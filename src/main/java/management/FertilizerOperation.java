
package management;

import crop.Crop;
import java.time.MonthDay;

/**
 *
 * @author Nathan Lighthart
 */
public class FertilizerOperation extends ManagementOperation {
    private final Fertilizer fertilizer;
    private final double fertilizerAmount;

    public FertilizerOperation(int managementId, Crop crop, MonthDay date,
            Fertilizer fertilizer, double fertilizerAmount) {
        super(managementId, crop, date);
        this.fertilizer = fertilizer;
        this.fertilizerAmount = fertilizerAmount;
    }

    public Fertilizer getFertilizer() {
        return fertilizer;
    }

    public double getFertilizerAmount() {
        return fertilizerAmount;
    }
}
