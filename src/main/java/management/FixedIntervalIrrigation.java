
package management;

import java.time.MonthDay;

/**
 *
 * @author Nathan Lighthart
 */
public class FixedIntervalIrrigation extends Irrigation {
    private final MonthDay start;
    private final MonthDay end;
    private final int interval;

    public FixedIntervalIrrigation(int irrigationId, double rate, double depth, MonthDay start, MonthDay end, int interval) {
        super(irrigationId, rate, depth);
        this.start = start;
        this.end = end;
        this.interval = interval;
    }

    public MonthDay getStart() {
        return start;
    }

    public MonthDay getEnd() {
        return end;
    }

    public int getInterval() {
        return interval;
    }
}
