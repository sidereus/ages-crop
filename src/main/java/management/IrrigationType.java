
package management;

/**
 *
 * @author Nathan Lighthart
 */
public enum IrrigationType {
    FIXED_DATE_CROP,
    ACTUAL_DATE,
    FIXED_INTERVAL,
    DEPLETION_LEVEL;

    public int getValue() {
        return ordinal() + 1;
    }
}
