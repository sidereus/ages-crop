
package management;

import java.time.MonthDay;

/**
 *
 * @author Nathan Lighthart
 */
public class FixedCropDateIrrigation extends Irrigation {
    private final int cropId;
    private final MonthDay date;

    public FixedCropDateIrrigation(int irrigationId, double rate, double depth, int cropId, MonthDay date) {
        super(irrigationId, rate, depth);
        this.cropId = cropId;
        this.date = date;
    }

    public int getCropId() {
        return cropId;
    }

    public MonthDay getDate() {
        return date;
    }
}
