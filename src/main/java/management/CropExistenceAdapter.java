
package management;

import crop.Crop;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

/**
 *
 * @author Nathan Lighthart
 */
public class CropExistenceAdapter extends AnnotatedAdapter {
    @Description("current crop")
    @Input private Crop crop;

    @Description("current management operation")
    @Input private ManagementOperation management;

    @Description("flag indicating that the crop exists")
    @Optional
    @Input @Output private boolean cropExists;

    @Override
    protected void run(Context context) {
        if (crop == null) {
            cropExists = false;
        }
        cropExists = doesCropExist();
    }

    private boolean doesCropExist() {
        if (CropExistence.isPerennialCrop(crop.idc)) {
            return true;
        } else {
            return doesNonPerennialCropExist();
        }
    }

    private boolean doesNonPerennialCropExist() {
        if (management != null) {
            if (management instanceof PlantOperation) {
                return true;
            } else if (management instanceof HarvestOperation) {
                HarvestOperation harvestOperation = (HarvestOperation) management;
                return CropExistence.doesHarvestLeaveCrop(harvestOperation.getHarvestType());
            }
        }
        // if no management or management is not plant or harvest leave cropExists as is
        return cropExists;
    }
}
