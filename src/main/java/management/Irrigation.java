
package management;

/**
 *
 * @author Nathan Lighthart
 */
public abstract class Irrigation {
    private final int irrigationId;
    private final double rate;
    private final double depth;

    public Irrigation(int irrigationId, double rate, double depth) {
        this.irrigationId = irrigationId;
        this.rate = rate;
        this.depth = depth;
    }

    public int getIrrigationId() {
        return irrigationId;
    }

    public double getRate() {
        return rate;
    }

    public double getDepth() {
        return depth;
    }
}
