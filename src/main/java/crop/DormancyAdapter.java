/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package crop;

import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.util.logging.Logger;

@Description("Add Dormancy module definition here")
@Author(name = "Olaf David, Manfred Fink, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Crop")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/crop/Dormancy.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/crop/Dormancy.xml")
public class DormancyAdapter extends AnnotatedAdapter {
    private static final Logger log
            = Logger.getLogger("oms3.model." + DormancyAdapter.class.getSimpleName());

    @Description("Maximum sunshine duration")
    @Units("h")
    @Input public double sunhmax;

    @Description("Entity Latidute")
    @Units("deg")
    @Input public double latitude;

    @Description("crop")
    @Input public Crop crop;

    @Description("Mean Temperature")
    @Units("C")
    @Input public double tmean;

    @Description("Fraction of actual potential heat units sum")
    @Optional
    @Input public double FPHUact;

    @Description("Minimum yearly sunshine duration")
    @Units("h")
    @Optional
    @Input @Output public double sunhmin;

    @Description("indicates dormancy of plants")
    @Output public boolean dormancy;

    @Override
    protected void run(Context context) {
        Dormancy component = new Dormancy();

        double tbase;
        if (crop == null) {
            tbase = 0.0;
        } else {
            tbase = crop.tbase;
        }

        component.sunhmax = sunhmax;
        component.latitude = latitude;
        component.tbase = tbase;
        component.tmean = tmean;
        component.FPHUact = FPHUact;
        component.sunhmin = sunhmin;

        component.execute();

        sunhmin = component.sunhmin;
        dormancy = component.dormancy;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
