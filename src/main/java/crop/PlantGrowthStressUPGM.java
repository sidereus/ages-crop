/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package crop;

public class PlantGrowthStressUPGM {
    public double aTransp;
    public double pTransp;
    public double BioNOpt;
    public double BioNAct;
    public double nstrs;
    public double wstrs;

    public void execute() {
        // calculate water stress
        if (pTransp > 0) {
            wstrs = 1 - ((aTransp) / (pTransp));
        } else {
            wstrs = 0;
        }

        // calculate N stress
        double phi_nit = 0;

        if (((BioNAct + 0.01) / (BioNOpt + 0.01)) >= 0.319) {
            phi_nit = 200 * (((BioNAct + 0.01) / (BioNOpt + 0.01)) - 0.319);
        }
        nstrs = 1 - (phi_nit / (phi_nit + Math.exp(3.535 - (0.02597 * phi_nit))));
        nstrs = (nstrs > 1.0) ? 1.0 : nstrs;
    }
}
