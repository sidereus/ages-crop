/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package crop;

public class PotentialCropGrowthUPGM {
    public double area;
    public int harvesttype;
    public boolean plantExisting;
    public boolean doHarvest;
    public double bn1;
    public double bn2;
    public double bn3;
    public int idc;
    public double cnyld;
    public boolean plantStateReset;
    public double BioNOpt;
    public double BioAct;
    public double FNPlant;
    public double BioNAct;
    public double FPHUact;
    public double Addresidue_pooln;
    public double BioYield;
    public double NYield;
    public double NYield_ha;

    public void execute() {
        NYield = 0;

        if (plantExisting) {
            // check to see if this is the first step where crop is calculated
            if (BioNAct == 0) {
                BioNAct = bn1;  // initial N content using optimal N content at emergence
            }

            calc_nuptake();
            Addresidue_pooln = 0;

            if (doHarvest) {
                calc_cropyield();
                calc_cropyield_ha();
                calc_residues();
                doHarvest = false;
            }
        } else if (plantStateReset) {   // called if there is no crop grown on HRU
            FNPlant = 0;
            BioNOpt = 0;
            BioNAct = 0;
            Addresidue_pooln = 0;
            plantStateReset = false;
        }

        /*
		 * Don't reset if plant is clover (IDC = 3), hay/pasture/wetlands (IDC = 6), or orchard/forest (IDC = 7).
		 * Reset all other crops the next time step.
         */
        plantStateReset = (idc == 3 || idc == 6 || idc == 7) ? false : true;
    }

    /* Calculates optimal biomass N content, not actual N uptake.
	 * Maturity is reached when fphu_act = 1, and then no further calculation is needed.
	 * Nutrients, water uptake, and transpiration should stop depending on the condition fphu = 1.
	 * Need to calculate water uptake by plants, potential water uptake, and nutrient uptake by plants.
     */
    private boolean calc_nuptake() {
        // fraction of N in plant biomass as a function of growth stage given optimal conditions
        if (bn1 > bn2 && bn2 > bn3 && bn3 > 0) {
            double s1 = Math.log((0.5 / (1 - ((bn2 - bn3) / (bn1 - bn3)))) - 0.5);
            double s2 = Math.log((1 / (1 - ((0.0001) / (bn1 - bn3)))) - 1);
            double n2 = (s1 - s2) / 0.5;
            double n1 = Math.log((0.5 / (1 - ((bn2 - bn3) / (bn1 - bn3)))) - 0.5) + (n2 * 0.5);
            FNPlant = ((bn1 - bn3) * (1 - (FPHUact / (FPHUact + Math.exp(n1 - n2 * FPHUact))))) + bn3;
            FNPlant = (harvesttype == 2) ? bn3 : FNPlant;
        } else {
            FNPlant = 0.01;
        }
        BioNOpt = FNPlant * BioAct;
        return true;
    }

    /* N fixation occurs when NO3-N levels in the root zone are insufficient to meet demand.
	 * Note: need to calculate phosphorus uptake.
     */
    private boolean calc_cropyield() {
        if (idc == 3 || idc == 6 || idc == 7 || (idc == 8)) {
            // amount of nitrogen (kg N/ha) to be removed from the HRU
            NYield = BioNAct * (BioYield / BioAct);
            NYield = (NYield < 0) ? 0 : NYield;

            BioNAct -= NYield;
            BioNAct = (BioNAct < 0) ? 0 : BioNAct;  // crop N is always present
        } else {
            /*
			 * For harvesting, four codes are implemented:
			 * 1 --> assumes harvesting with main crop and a cover crop; plant growth stopped
			 * 2 --> assumes harvesting with main crop; the cover crop remains on the field, plant growth stopped (former kill op)
			 * 3 --> assumes harvesting with main crop and a cover crop; plant growth continues (may not be suitable for meadows)
			 * 4 --> assumes harvesting with main crop; plant growth continues
			 *
			 * If harvest is determined by code 1, the above-ground plant biomass is removed as dry economic yield.
			 * For majority of crops, harvest index is between 0 and 1; however, for plants whose roots are harvested
			 * the harvest index may be > 1. Harvest index is calculated for each day of growing season.
             */
            NYield = cnyld * BioYield;
            if (NYield > BioNAct * (NYield / (NYield + ((BioAct - BioYield) * (bn3 / 2.0))))) {
                NYield = BioNAct * (NYield / (NYield + ((BioAct - BioYield) * (bn3 / 2.0))));
            }
        }
        return true;
    }

    private void calc_cropyield_ha() {
        NYield_ha = NYield * (area / 10000) / 10000;
    }

    private boolean calc_residues() {
        if (idc == 7) {
            Addresidue_pooln = NYield;
        } else if (idc == 1 || idc == 2 || idc == 4 || idc == 5) {
            Addresidue_pooln = BioNAct - NYield;
        } else if (idc == 6 || idc == 3) {
            Addresidue_pooln = NYield * 0.1;
            Addresidue_pooln = Math.min(Addresidue_pooln, BioNAct);
            BioNAct -= Addresidue_pooln;
        } else if (idc == 8) {
            Addresidue_pooln = NYield * 0.1;
            Addresidue_pooln = Math.min(Addresidue_pooln, BioNAct);
            BioNAct -= Addresidue_pooln;
        }
        return true;
    }
}
