/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package crop;

import ages.types.HRU;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Keywords;
import annotations.License;
import annotations.Range;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.time.LocalDate;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import management.HarvestOperation;

@Description("Add PotentialCropGrowth module definition here")
@Author(name = "Olaf David, Manfred Fink, Robert Streetman, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Crop")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/crop/PotentialCropGrowth.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/crop/PotentialCropGrowth.xml")
public class PotentialCropGrowthAdapter extends AnnotatedAdapter {
    @Description("light extinction coefficient")
    @Role(PARAMETER)
    @Range(min = -1.0, max = 0.0)
    @Input public double LExCoef;

    @Description("Rootdepth Factor")
    @Role(PARAMETER)
    @Range(min = 0, max = 10)
    @Input public double rootfactor;

    @Description("Current Time")
    @Input public LocalDate time;

    @Description("Flag for UPGM Crop Growth Calculation Method")
    @Input public boolean flagUPGM;

    @Description("HRU")
    @Input public HRU hru;

    @Description("current crop")
    @Input public Crop crop;

    @Description("Mean Temperature")
    @Units("C")
    @Input public double tmean;

    @Description("Daily solar radiation")
    @Units("MJ/m2/day")
    @Input public double solRad;

    @Description("Dormancy Status")
    @Input public boolean dormancy;

    @Description("Max root depth")
    @Units("m")
    @Input public double soil_root;

    @Description("Flag plant existing")
    @Input public boolean cropExists;

    @Description("next harvesting date")
    @Input public LocalDate nextHarvestingDate;

    @Description("next harvesting operation")
    @Input public HarvestOperation nextHarvestingOperation;

    @Description("Reset plant state variables")
    @Optional
    @Input @Output public boolean plantStateReset;

    @Description("Array of state variables LAI ")
    @Input @Output public double LAI;

    @Description("Biomass above ground on the day of harvest")
    @Units("kg/ha")
    @Optional
    @Input @Output public double BioagAct;

    @Description("Actual Heat Units Achieved")
    @Optional
    @Input @Output public double PHUact;

    @Description("Optimal Biomass N Content")
    @Units("kgN/ha")
    @Optional
    @Input @Output public double BioNOpt;

    @Description("Daily fraction of max LAI")
    @Optional
    @Input @Output public double frLAImxAct;

    @Description("Daily fraction of max LAI")
    @Optional
    @Input @Output public double frLAImx_xi;

    @Description("Daily fraction of max root development")
    @Optional
    @Input @Output public double frRootAct;

    @Description("Actual Crop Biomass")
    @Units("kg/ha")
    @Optional
    @Input @Output public double BioAct;

    @Description("Actual Canopy Height")
    @Units("m")
    @Optional
    @Input @Output public double CanHeightAct;

    @Description("Actual Root Depth")
    @Units("m")
    @Input @Output public double zrootd;

    @Description("Fraction of N in crop biomass under optimal conditions")
    @Optional
    @Input @Output public double FNPlant;

    @Description("Daily biomass increase")
    @Units("kg/ha")
    @Optional
    @Input @Output public double BioOpt_delta;

    @Description("Actual Biomass N Content")
    @Units("kgN/ha")
    @Optional
    @Input @Output public double BioNAct;

    @Description("Harvest Index [0-1]")
    @Optional
    @Input @Output public double HarvIndex;

    @Description("Fraction of Total Required Heat Units Achieved")
    @Optional
    @Input @Output public double FPHUact;

    @Description("Number of fertilisation action in crop")
    @Optional
    @Input @Output public double nfert;

    @Description("Biomass Added to Residue Pool After Harvesting")
    @Units("kg/ha")
    @Optional
    @Input @Output public double Addresidue_pool;

    @Description("Yield Biomass")
    @Units("kg/ha")
    @Optional
    @Input @Output public double BioYield;

    @Description("the daily atmospheric level of CO2")
    @Units("ppm")
    @Input public double co2;

    @Description("Amount N Added to Residue Pool After Harvesting")
    @Units("kgN/ha")
    @Output public double Addresidue_pooln;

    @Description("Amount of N in Yield")
    @Units("absolut")
    @Output public double NYield;

    @Description("Amount of N in Yield, per Hectare")
    @Units("kg N/ha")
    @Output public double NYield_ha;

    @Description("Base growth temperature")
    @Units("C")
    @Output public double tbase;

    @Description("Optimum growth temperature")
    @Units("C")
    @Output public double topt;

    @Description("N at fr3")
    @Output public double fr3N;

    @Description("DLAI (Senescence)")
    @Output public double dlai;

    @Description("Co2Effect")
    @Output public double co2effect;

    private Map<HRU, PotentialCropGrowthSWAT> swatComponentMap = new ConcurrentHashMap<>();

    @Override
    protected void run(Context context) {
        if (flagUPGM) {
            executeUPGM();
        } else {
            executeSWAT();
        }
    }

    private boolean doHarvest() {
        if (nextHarvestingDate == null || nextHarvestingOperation == null) {
            return false;
        }
        return time.plusDays(1L).equals(nextHarvestingDate);
    }

    private void executeSWAT() {
        PotentialCropGrowthSWAT component = swatComponentMap.get(hru);

        if (component == null) {
            //component = new PotentialCropGrowthSWAT();
            component = new PotentialCropGrowthSWAT();
            swatComponentMap.put(hru, component);
        }

        component.LExCoef = LExCoef;
        component.rootfactor = rootfactor;
        component.flagUPGM = flagUPGM;
        component.area = hru.area;
        component.tmean = tmean;
        component.solRad = solRad;
        component.dormancy = dormancy;
        component.soil_root = soil_root;
        component.plantExisting = cropExists;
        component.doHarvest = doHarvest();
        if (component.doHarvest) {
            component.harvesttype = nextHarvestingOperation.getHarvestType();
        }
        if (crop != null) {
            component.phu = crop.phu;
            component.idc = crop.idc;
            component.rue = crop.rue;
            component.hvsti = crop.hvsti;
            component.frgrw1 = crop.frgrw1;
            component.frgrw2 = crop.frgrw2;
            component.lai_min = crop.lai_min;
            component.mlai = crop.mlai;
            component.mlai1 = crop.laimx1;
            component.mlai2 = crop.laimx2;
            component.dlai = crop.dlai;
            component.chtmx = crop.chtmx;
            component.rdmx = crop.rdmx;
            component.topt = crop.topt;
            component.tbase = crop.tbase;
            component.cnyld = crop.cnyld;
            component.bn1 = crop.bn1;
            component.bn2 = crop.bn2;
            component.bn3 = crop.bn3;
            component.fr3N = crop.bn3;
        }
        component.plantStateReset = plantStateReset;
        component.LAI = LAI;
        component.BioagAct = BioagAct;
        component.PHUact = PHUact;
        component.BioNOpt = BioNOpt;
        component.frLAImxAct = frLAImxAct;
        component.frLAImx_xi = frLAImx_xi;
        component.frRootAct = frRootAct;
        component.BioAct = BioAct;
        component.CanHeightAct = CanHeightAct;
        component.zrootd = zrootd;
        component.FNPlant = FNPlant;
        component.BioOpt_delta = BioOpt_delta;
        component.BioNAct = BioNAct;
        component.HarvIndex = HarvIndex;
        component.FPHUact = FPHUact;
        component.nfert = nfert;

        // co2
        component.co2 = co2;

        component.execute();

        plantStateReset = component.plantStateReset;
        LAI = component.LAI;
        BioagAct = component.BioagAct;
        PHUact = component.PHUact;
        BioNOpt = component.BioNOpt;
        frLAImxAct = component.frLAImxAct;
        frLAImx_xi = component.frLAImx_xi;
        frRootAct = component.frRootAct;
        BioAct = component.BioAct;
        CanHeightAct = component.CanHeightAct;
        zrootd = component.zrootd;
        FNPlant = component.FNPlant;
        BioOpt_delta = component.BioOpt_delta;
        BioNAct = component.BioNAct;
        HarvIndex = component.HarvIndex;
        FPHUact = component.FPHUact;
        nfert = component.nfert;
        Addresidue_pool = component.Addresidue_pool;
        Addresidue_pooln = component.Addresidue_pooln;
        BioYield = component.BioYield;
        NYield = component.NYield;
        NYield_ha = component.NYield_ha;
        tbase = component.tbase;
        topt = component.topt;
        fr3N = component.fr3N;
        dlai = component.dlai;
        co2effect = component.co2eff;
    }

    private void executeUPGM() {
        PotentialCropGrowthUPGM component = new PotentialCropGrowthUPGM();

        component.area = hru.area;
        component.plantExisting = cropExists;
        component.doHarvest = doHarvest();
        if (component.doHarvest) {
            component.harvesttype = nextHarvestingOperation.getHarvestType();
        }
        if (crop != null) {
            component.idc = crop.idc;
            component.cnyld = crop.cnyld;
            component.bn1 = crop.bn1;
            component.bn2 = crop.bn2;
            component.bn3 = crop.bn3;
        }
        component.plantStateReset = plantStateReset;
        component.BioNOpt = BioNOpt;
        component.BioAct = BioAct;
        component.FNPlant = FNPlant;
        component.BioNAct = BioNAct;
        component.FPHUact = FPHUact;
        component.BioYield = BioYield;

        component.execute();

        plantStateReset = component.plantStateReset;
        BioNOpt = component.BioNOpt;
        FNPlant = component.FNPlant;
        BioNAct = component.BioNAct;
        Addresidue_pooln = component.Addresidue_pooln;
        NYield = component.NYield;
        NYield_ha = component.NYield_ha;
    }
}
